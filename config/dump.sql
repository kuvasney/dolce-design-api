-- MySQL dump 10.17  Distrib 10.3.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dolcedesign
-- ------------------------------------------------------
-- Server version	10.3.18-MariaDB-1:10.3.18+maria~bionic-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `name` varchar
(256) NOT NULL,
  `client` varchar
(256) DEFAULT NULL,
  `place` varchar
(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `job_type_id` int
(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp
(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp
() ON
UPDATE current_timestamp(),
  PRIMARY KEY
(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `
job`
VALUES
  (4, 'Obra linda', 'Lindo', 'Casa da mamae', 'ficou lindo', 3, '2019-10-08 12:08:29', '2019-10-08 15:08:29'),
  (5, 'Recanto Somasquinho', 'ONG', 'Santo André', 'obra feita', 3, '2019-10-08 12:09:12', '2019-10-08 15:09:12'),
  (6, 'Casa', 'Rafael', 'Santo André', 'reformei minha casa!', 4, '2019-10-08 13:24:21', '2019-10-08 16:24:21'),
  (7, 'Sala', 'Rafael', 'Santo André', 'reformei minha sala!', 6, '2019-10-08 13:24:34', '2019-10-08 16:24:34'),
  (8, 'Praia', 'Jessica', 'Itanhaem', 'reformei minha cozinha de praia', 3, '2019-10-08 13:24:59', '2019-10-08 16:24:59');
/*!40000 ALTER TABLE `job` ENABLE KEYS */;


--
-- Table structure for table `job_photos`
--

DROP TABLE IF EXISTS `job_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_photos`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar
(256) NOT NULL,
  `description` varchar
(1024) NOT NULL,
  `job_id` int
(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp
(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp
() ON
UPDATE current_timestamp(),
  PRIMARY KEY
(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_photos`
--

/*!40000 ALTER TABLE `job_photos` DISABLE KEYS */;
INSERT INTO `
job_photos`
VALUES
  (2, '../uploads//817458-decas.jpg', 'repetido', 5, '2019-10-08 13:04:37', '2019-10-08 16:04:37'),
  (3, 'ftd.jpeg', 'ra', 5, '2019-10-08 13:06:36', '2019-10-08 16:06:36'),
  (4, '846230-ftd.jpeg', 'ra', 5, '2019-10-08 13:10:11', '2019-10-08 16:10:11'),
  (5, '506091-ftd.jpeg', 'ra', 5, '2019-10-08 13:11:28', '2019-10-08 16:11:28'),
  (6, '107500-Captura de tela de 2019-09-18 16-03-08.png', 'ra', 5, '2019-10-08 13:16:13', '2019-10-08 16:16:13'),
  (7, '383933-Captura de tela de 2019-09-06 11-17-25.png', 'ra', 5, '2019-10-08 13:16:29', '2019-10-08 16:16:29'),
  (8, '405022-oditempalrmera.png', 'rara', 7, '2019-10-08 13:29:51', '2019-10-08 16:29:51'),
  (9, '481951-camisa-do-palmeiras-i-2017.jpg', 'repetido', 8, '2019-10-08 19:19:14', '2019-10-08 22:19:14');
/*!40000 ALTER TABLE `job_photos` ENABLE KEYS */;

--
-- Table structure for table `job_type`
--

DROP TABLE IF EXISTS `job_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_type`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `title` varchar
(256) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp
(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp
() ON
UPDATE current_timestamp(),
  PRIMARY KEY
(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_type`
--

/*!40000 ALTER TABLE `job_type` DISABLE KEYS */;
INSERT INTO `
job_type`
VALUES
  (3, 'cozinha', '2019-10-01 15:50:05', '2019-10-01 18:50:05'),
  (4, 'dormitorio', '2019-10-08 13:23:19', '2019-10-08 16:23:19'),
  (5, 'area de serviço', '2019-10-08 13:23:29', '2019-10-08 16:23:29'),
  (6, 'sala', '2019-10-08 13:23:33', '2019-10-08 16:23:33');
/*!40000 ALTER TABLE `job_type` ENABLE KEYS */;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `testimonial` text NOT NULL,
  `author` varchar
(256) NOT NULL,
  `job` varchar
(256) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp
(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp
() ON
UPDATE current_timestamp(),
  PRIMARY KEY
(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `
testimonials`
VALUES
  (1, 'Qualidade e Excelência em tudo que faz, os mínimos detalhes pensados para deixar um ambiente bonito e agradável, Parabéns.', 'Renato Fonseca', 'Marmoraria Fonseca', '2019-09-25 21:16:07', '2019-09-26 00:16:07'),
  (2, 'teste', 'Rafael', 'Casa', '2019-09-25 21:52:44', '2019-09-26 00:52:44'),
  (3, 'teste', 'Rafael', 'Casa', '2019-09-25 21:53:22', '2019-09-26 00:53:22');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `email` varchar
(256) CHARACTER
SET latin1
NOT NULL,
  `password` varchar
(2048) CHARACTER
SET latin1
NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp
(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp
() ON
UPDATE current_timestamp(),
  PRIMARY KEY
(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `
users`
VALUES
  (1, 'rafael@contato.abc.br', '$2y$10$G8.Jh0hjyACDZSxolWyjW.h.ANkO4jBh5rrgjlp/z74nDIbZvAXjC', '2019-09-18 11:16:17', '2019-09-18 14:16:17'),
  (2, 'contato@rafael.abc.br', '$2y$10$N71l03ZA1.ps3neSI2tkuewOsec6m5z/JjcPebqqQ9ihqsqFahd66', '2019-09-19 12:38:04', '2019-09-19 15:38:04');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

--
-- Dumping routines for database 'dolcedesign'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-08 19:29:07
