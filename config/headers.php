<?php
// required headers
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') { 
  header('Access-Control-Allow-Origin: *'); 
  header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS'); 
  header("Access-Control-Allow-Headers: *");
  header('Access-Control-Max-Age: 1728000'); 
  header('Content-Length: 0'); 
  // header('Content-Type: text/plain'); 
  header('Content-type: application/json');
  die(); 
} 
header('Access-Control-Allow-Origin: *'); 
header('Content-Type: application/json'); 

date_default_timezone_set('America/Sao_Paulo');
?>