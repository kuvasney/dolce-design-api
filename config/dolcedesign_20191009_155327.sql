-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- DROP DATABASE "dolcedesign" -----------------------------
DROP DATABASE IF EXISTS `dolcedesign`;
-- ---------------------------------------------------------


-- CREATE DATABASE "dolcedesign" ---------------------------
CREATE DATABASE `dolcedesign` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `dolcedesign`;
-- ---------------------------------------------------------


-- CREATE TABLE "job" ------------------------------------------
CREATE TABLE `job` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`client` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`place` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`description` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`job_type_id` Int( 11 ) NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 9;
-- -------------------------------------------------------------


-- CREATE TABLE "job_photos" -----------------------------------
CREATE TABLE `job_photos` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`filename` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`description` VarChar( 1024 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`job_id` Int( 11 ) NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 15;
-- -------------------------------------------------------------


-- CREATE TABLE "job_type" -------------------------------------
CREATE TABLE `job_type` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`title` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------


-- CREATE TABLE "testimonials" ---------------------------------
CREATE TABLE `testimonials` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`testimonial` Text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`author` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`job` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE `users` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`email` VarChar( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`password` VarChar( 2048 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- Dump data of "job" --------------------------------------
INSERT INTO `job`(`id`,`name`,`client`,`place`,`description`,`job_type_id`,`created`,`modified`) VALUES 
( '4', 'Obra linda', 'Lindo', 'Casa da mamae', 'ficou lindo', '3', '2019-10-08 12:08:29', '2019-10-08 15:08:29' ),
( '5', 'Recanto Somasquinho', 'ONG', 'Santo André', 'obra feita', '3', '2019-10-08 12:09:12', '2019-10-08 15:09:12' ),
( '6', 'Casa', 'Rafael', 'Santo André', 'reformei minha casa!', '4', '2019-10-08 13:24:21', '2019-10-08 16:24:21' ),
( '7', 'Sala', 'Rafael', 'Santo André', 'reformei minha sala!', '6', '2019-10-08 13:24:34', '2019-10-08 16:24:34' ),
( '8', 'Praia', 'Jessica', 'Itanhaem', 'reformei minha cozinha de praia', '3', '2019-10-08 13:24:59', '2019-10-08 16:24:59' );
-- ---------------------------------------------------------


-- Dump data of "job_photos" -------------------------------
INSERT INTO `job_photos`(`id`,`filename`,`description`,`job_id`,`created`,`modified`) VALUES 
( '10', '201033-fluxo_flux.png', 'repetido', '5', '2019-10-09 18:26:43', '2019-10-09 18:26:43' ),
( '11', '606032-filezilla.png', 'ra', '5', '2019-10-09 18:27:23', '2019-10-09 18:27:23' ),
( '12', '56394-Angel Dust.jpg', 'rara', '4', '2019-10-09 18:27:30', '2019-10-09 18:27:30' ),
( '13', '543611-SP.jpg', 'ra', '7', '2019-10-09 18:27:43', '2019-10-09 18:27:43' ),
( '14', '720987-37864.jpg', 'Bonito', '8', '2019-10-09 18:51:49', '2019-10-09 18:51:49' );
-- ---------------------------------------------------------


-- Dump data of "job_type" ---------------------------------
INSERT INTO `job_type`(`id`,`title`,`created`,`modified`) VALUES 
( '3', 'cozinha', '2019-10-01 15:50:05', '2019-10-01 18:50:05' ),
( '4', 'dormitorio', '2019-10-08 13:23:19', '2019-10-08 16:23:19' ),
( '5', 'area de serviço', '2019-10-08 13:23:29', '2019-10-08 16:23:29' ),
( '6', 'sala', '2019-10-08 13:23:33', '2019-10-08 16:23:33' );
-- ---------------------------------------------------------


-- Dump data of "testimonials" -----------------------------
INSERT INTO `testimonials`(`id`,`testimonial`,`author`,`job`,`created`,`modified`) VALUES 
( '1', 'Qualidade e Excelência em tudo que faz, os mínimos detalhes pensados para deixar um ambiente bonito e agradável, Parabéns.', 'Renato Fonseca', 'Marmoraria Fonseca', '2019-09-25 21:16:07', '2019-09-26 00:16:07' ),
( '2', 'teste', 'Rafael', 'Casa', '2019-09-25 21:52:44', '2019-09-26 00:52:44' ),
( '3', 'teste', 'Rafael', 'Casa', '2019-09-25 21:53:22', '2019-09-26 00:53:22' );
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
INSERT INTO `users`(`id`,`email`,`password`,`created`,`modified`) VALUES 
( '1', 'rafael@contato.abc.br', '$2y$10$G8.Jh0hjyACDZSxolWyjW.h.ANkO4jBh5rrgjlp/z74nDIbZvAXjC', '2019-09-18 11:16:17', '2019-09-18 14:16:17' ),
( '2', 'contato@rafael.abc.br', '$2y$10$N71l03ZA1.ps3neSI2tkuewOsec6m5z/JjcPebqqQ9ihqsqFahd66', '2019-09-19 12:38:04', '2019-09-19 15:38:04' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


