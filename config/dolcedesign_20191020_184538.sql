-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- DROP TABLE "job" --------------------------------------------
DROP TABLE IF EXISTS `job` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "job" ------------------------------------------
CREATE TABLE `job` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`client` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`place` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`description` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`job_type_id` Int( 11 ) NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- DROP TABLE "job_photos" -------------------------------------
DROP TABLE IF EXISTS `job_photos` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "job_photos" -----------------------------------
CREATE TABLE `job_photos` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`filename` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`description` VarChar( 1024 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`job_id` Int( 11 ) NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`highlight` TinyInt( 255 ) NOT NULL DEFAULT 0,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 11;
-- -------------------------------------------------------------


-- DROP TABLE "job_type" ---------------------------------------
DROP TABLE IF EXISTS `job_type` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "job_type" -------------------------------------
CREATE TABLE `job_type` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`title` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- DROP TABLE "testimonials" -----------------------------------
DROP TABLE IF EXISTS `testimonials` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "testimonials" ---------------------------------
CREATE TABLE `testimonials` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`testimonial` Text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`author` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`job` VarChar( 256 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- DROP TABLE "users" ------------------------------------------
DROP TABLE IF EXISTS `users` CASCADE;
-- -------------------------------------------------------------


-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE `users` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`email` VarChar( 256 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`password` VarChar( 2048 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`created` DateTime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`modified` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- Dump data of "job" --------------------------------------
INSERT INTO `job`(`id`,`name`,`client`,`place`,`description`,`job_type_id`,`created`,`modified`) VALUES 
( '1', 'Máximus', 'Máximus ', 'Ribeirão Pires', 'Criamos um espaço corporativo para receber parceiros com muita personalidade. Ar industrial e moderno, tudo de bom.', '1', '2019-10-18 23:18:33', '2019-10-18 23:18:33' ),
( '2', 'Leve &amp; Moderno', 'Favoretto', 'Pq. das Nações, Santo André', 'Um casal que queria conforto, modernidade e sensação de  &quot;se sentir em casa&quot;. Nos inspiramos na leveza do voo de um pássaro.', '2', '2019-10-18 23:19:14', '2019-10-18 23:19:14' );
-- ---------------------------------------------------------


-- Dump data of "job_photos" -------------------------------
INSERT INTO `job_photos`(`id`,`filename`,`description`,`job_id`,`created`,`modified`,`highlight`) VALUES 
( '1', '890128-_mg_1467.jpg', 'Área comum da empresa', '1', '2019-10-18 23:43:06', '2019-10-19 00:06:07', '1' ),
( '2', '907655-_mg_1476.jpg', 'Vista alternativa da área da empresa', '1', '2019-10-18 23:43:58', '2019-10-19 00:06:14', '0' ),
( '3', '686823-_mg_1522.jpg', 'Local de reuniões decorado de acordo com as ideias do cliente', '1', '2019-10-18 23:44:50', '2019-10-19 00:06:21', '0' ),
( '4', '185275-vilhora_0070.jpg', 'O quarto do casal', '2', '2019-10-19 00:10:30', '2019-10-20 16:49:23', '1' ),
( '5', '57567-vilhora_0061.jpg', 'Banheiro dela', '2', '2019-10-19 00:10:49', '2019-10-20 16:49:20', '0' ),
( '6', '297499-vilhora_0056.jpg', 'Banheiro dele', '2', '2019-10-19 00:11:05', '2019-10-19 00:11:05', '0' ),
( '7', '13544-vilhora_0039.jpg', 'Pássaros em destaque na sala', '2', '2019-10-19 00:11:35', '2019-10-20 16:48:57', '0' ),
( '8', '494283-vilhora_0054.jpg', 'Cozinha', '2', '2019-10-19 00:11:47', '2019-10-19 00:11:47', '0' ),
( '9', '645009-vilhora_0036.jpg', 'Pássaro', '2', '2019-10-19 00:11:57', '2019-10-19 00:11:57', '0' ),
( '10', '484175-vilhora_0038.jpg', 'Detalhes de pássaros', '2', '2019-10-19 00:12:25', '2019-10-19 00:12:25', '0' );
-- ---------------------------------------------------------


-- Dump data of "job_type" ---------------------------------
INSERT INTO `job_type`(`id`,`title`,`created`,`modified`) VALUES 
( '1', 'corporativo', '2019-10-18 23:17:59', '2019-10-18 23:17:59' ),
( '2', 'apartamento residencial', '2019-10-18 23:18:22', '2019-10-18 23:18:22' );
-- ---------------------------------------------------------


-- Dump data of "testimonials" -----------------------------
INSERT INTO `testimonials`(`id`,`testimonial`,`author`,`job`,`created`,`modified`) VALUES 
( '1', 'Qualidade e Excelência em tudo que faz, os mínimos detalhes pensados para deixar um ambiente bonito e agradável, Parabéns.', 'Renato Fonseca', 'Marmoraria Fonseca', '2019-09-25 21:16:07', '2019-09-26 00:16:07' ),
( '2', 'Excelente projeto criado , acompanhado, executado e entregue. Conseguiu captar, conciliar e entregar ideias para um projeto exclusivo com muita inovação, bom gosto e funcional!!', 'Alex Favoretto', 'Cliente residencial', '2019-10-20 17:00:08', '2019-10-20 17:00:08' );
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
INSERT INTO `users`(`id`,`email`,`password`,`created`,`modified`) VALUES 
( '1', 'rafael@contato.abc.br', '$2y$10$G8.Jh0hjyACDZSxolWyjW.h.ANkO4jBh5rrgjlp/z74nDIbZvAXjC', '2019-09-18 11:16:17', '2019-09-18 14:16:17' ),
( '2', 'contato@rafael.abc.br', '$2y$10$N71l03ZA1.ps3neSI2tkuewOsec6m5z/JjcPebqqQ9ihqsqFahd66', '2019-09-19 12:38:04', '2019-09-19 15:38:04' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


