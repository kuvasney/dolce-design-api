<?php
// required headers
// include_once '../config/headers.php';
 
// // include database and object files
// include_once '../config/db.php';
// include_once '../objects/testimonial.php';
 
// // get database connection
// $database = new Database();
// $db = $database->getConnection();
 
// // prepare testimonial object
// $testimonial = new testimonial($db);
 
// // get id of testimonial to be edited
// $data = json_decode(file_get_contents("php://input"));
 
// // set ID property of testimonial to be edited
// $testimonial->id = $data->id;
 
// // set testimonial property values
// $testimonial->testimonial = $data->testimonial;
// $testimonial->author = $data->author;
// $testimonial->job = $data->job;
 
// // update the testimonial
// if($testimonial->update()){
 
//     // set response code - 200 ok
//     http_response_code(200);
 
//     // tell the user
//     echo json_encode(array("message" => "depoimento atualizado."));
// }
 
// // if unable to update the testimonial, tell the user
// else{
 
//     // set response code - 503 service unavailable
//     http_response_code(503);
 
//     // tell the user
//     echo json_encode(array("message" => "Não foi possível atualizar o depoimento."));
// }
?>