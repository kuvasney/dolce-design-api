<?php
// required headers
include_once '../config/headers.php';

// get database connection
include_once '../config/db.php';

// instantiate job_type object
include_once '../objects/job_photos.php';

$database = new Database();
$db = $database->getConnection();
 
$jobPhotos = new JobPhotos($db);
 
// get posted data
$data = (object) $_POST;

// upload itens
$response = array();
$upload_dir = '../uploads';
$upload_folder = 'uploads';
// if(!is_dir($upload_dir)) mkdir($upload_dir);
$server_url = 'http://api.dolce-d.com.br:8888';

// upload proccess
if($_FILES['photo']) {
    $photo_name = $_FILES["photo"]["name"];
    $photo_tmp_name = $_FILES["photo"]["tmp_name"];
    $error = $_FILES["photo"]["error"];
    if($error > 0) {
        $response = array(
            "status" => "error",
            "error" => true,
            "message" => "Error uploading the file!"
        );
    }else {
        $random_name = rand(1000,1000000)."-".$photo_name;
        $upload_name = $upload_dir."/".strtolower($random_name);
        $upload_name = preg_replace('/\s+/', '-', $upload_name);
        $moved = move_uploaded_file($photo_tmp_name , __DIR__ . '/' . $upload_name);
        if($moved) {

            
            // set job property values
            $jobPhotos->filename = strtolower($random_name);
            $jobPhotos->description = $data->description;
            $jobPhotos->highlight = $data->highlight;
            $jobPhotos->job_id = $data->job_id;
            $jobPhotos->created = date('Y-m-d H:i:s');
        
            // create the job
            if($jobPhotos->create()) {
        
                // set response code - 201 created
                http_response_code(201);
        
                // tell the user
                // echo json_encode(array("message" => "arquivo postado."));
                $response = array(
                    "status" => "success",
                    "error" => false,
                    "message" => "File uploaded successfully",
                    "url" => $upload_name,
                    "highlight" => $data->highlight,
                    "job_id" => $data->job_id,
                    "description" => $data->description
                );
            }
        
            // if unable to create the job, tell the user
            else {
        
                // set response code - 503 service unavailable
                http_response_code(503);
        
                // tell the user
                $response = array(
                    "status" => "error",
                    "error" => true,
                    "message" => "Error creating the file!",
                    "url" => $server_url."/".$upload_name,
                    "job_id" => $data->job_id,
                    "description" => $data->description
                );    
            }            
            
                   
        }else {
            $response = array(
                "status" => "error",
                "error" => true,
                "message" => "Error uploading the file!",
                "job_id" => $job_id,
                "description" => $description
            );
        }
    }
    
}else {
    $response = array(
        "status" => "error",
        "error" => true,
        "message" => "No file was sent!"
    );
}


echo json_encode($response);
?>