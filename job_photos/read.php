<?php
// // required headers
// include_once '../config/headers.php';
 
// // include database and object files
// include_once '../config/db.php';
// include_once '../objects/testimonial.php';
 
// // instantiate database and testimonial object
// $database = new Database();
// $db = $database->getConnection();
 
// // initialize object
// $testimonial = new testimonial($db);
 
// // query testimonials
// $stmt = $testimonial->read();
// $num = $stmt->rowCount();
 
// // check if more than 0 record found
// if($num>0){
 
//     // testimonials array
//     $testimonials_arr=array();
//     $testimonials_arr["records"]=array();
 
//     // retrieve our table contents
//     // fetch() is faster than fetchAll()
//     // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
//     while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
//         // extract row
//         // this will make $row['name'] to
//         // just $name only
//         extract($row);
 
//         $testimonial_item=array(
//             "id" => $id,
//             "testimonial" => html_entity_decode($testimonial),
//             "author" => html_entity_decode($author),
//             "job" => $job,
//             "created_at" => $created,
//             "modified_at" => $modified
//         );
 
//         array_push($testimonials_arr["records"], $testimonial_item);
//     }
 
//     // set response code - 200 OK
//     http_response_code(200);
 
//     // show testimonials data in json format
//     echo json_encode($testimonials_arr);

// } else{
 
//     // set response code - 404 Not found
//     http_response_code(404);
 
//     // tell the user no testimonials found
//     echo json_encode(
//         array("message" => "No testimonials found.")
//     );
}