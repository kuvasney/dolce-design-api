<?php
// required headers
include_once '../config/headers.php';
 
// include database and object files
include_once '../config/db.php';
include_once '../objects/job.php';
 
// instantiate database and job object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$job = new job($db);
 
// query jobs
$stmt = $job->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // jobs array
    $jobs_arr=array();
    $jobs_arr["jobs"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['shortname'] to
        // just $shortname only
        extract($row);
 
        $job_item=array(
            "id" => $id,
            "name" => html_entity_decode($name),
            "client" => html_entity_decode($client),
            "place" => html_entity_decode($place),
            "description" => html_entity_decode($description),
            "date" => $created,
            "job_type" => $title,
            "photo" => $filename
        );
 
        array_push($jobs_arr["jobs"], $job_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show jobs data in json format
    echo json_encode($jobs_arr);

} else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no jobs found
    echo json_encode(
        array("message" => "No jobs found.")
    );
}