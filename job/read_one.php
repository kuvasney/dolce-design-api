<?php
// required headers
include_once '../config/headers.php';
 
// include database and object files
include_once '../config/db.php';
include_once '../objects/job.php';
 
// instantiate database and job object
$database = new Database();
$db = $database->getConnection();
 
// prepare product object
$job = new Job($db);
 
// set ID property of record to read
$job->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of product to be edited
$job->readOne();

if($job->id!=null){    
    $job_photos = $job->photos;

    $job_item = array(
        "id" => html_entity_decode($job->id),
        "name" => html_entity_decode($job->name),
        "client" => html_entity_decode($job->client),
        "place" => html_entity_decode($job->place),
        "description" => html_entity_decode($job->description),
        "job_type" => html_entity_decode($job->title),
        "photos" => $job_photos
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // print_r($job);
    // make it json format
    echo json_encode($job_item);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user product does not exist
    echo json_encode(array("message" => "Obra não encontrada."));
}
?>