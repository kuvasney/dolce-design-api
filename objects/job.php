<?php
class Job{
 
    // database connection and table name
    private $conn;
    private $table_name = "job";
 
    // object properties
    public $id;
    public $name;
    public $client;
    public $place;
    public $description;
    public $job_type_id;
    public $created;
    public $photos;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read job
    function read(){
    
        //select all data
        $query = "SELECT j.id, j.name, j.client, j.place, j.description, j.job_type_id, j.created, t.title, jp.filename
                FROM " . $this->table_name . " j
                INNER JOIN job_type t ON j.job_type_id = t.id
                INNER JOIN job_photos jp ON j.id = jp.job_id AND jp.highlight = 1
                ORDER BY id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // create product
    function create(){
    
        // query to insert record
        $query = "INSERT INTO " . $this->table_name . " SET name=:name, client=:client, place=:place, description=:description, job_type_id=:job_type_id, created=:created";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->client=htmlspecialchars(strip_tags($this->client));
        $this->place=htmlspecialchars(strip_tags($this->place));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->job_type=htmlspecialchars(strip_tags($this->job_type_id));
        $this->created=htmlspecialchars(strip_tags($this->created));
    
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":client", $this->client);
        $stmt->bindParam(":place", $this->place);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":job_type_id", $this->job_type_id);
        $stmt->bindParam(":created", $this->created);

        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    // used when filling up the update product form
    function readOne(){
    
        $query = "SELECT j.id, j.name, j.client, j.place, j.job_type_id, jp.filename, jp.description, jp.job_id
                FROM job_photos AS jp 
                JOIN ".$this->table_name." AS j  ON " .$this->id. " = jp.job_id 
                WHERE  j.id = :idJob
                ";

        $queryJob = "SELECT j.id, j.name, j.client, j.place, j.description, j.job_type_id, t.title
                FROM job AS j 
                INNER JOIN job_type t ON j.job_type_id = t.id
                WHERE  j.id = :idJob
                ";
        $stmtJob = $this->conn->prepare( $queryJob );
        $stmtJob->bindParam(":idJob", $this->id);
        $stmtJob->execute();
        $rowJob = $stmtJob->fetch(PDO::FETCH_ASSOC);
        
        $this->id = $rowJob['id'];
        $this->name = $rowJob['name'];
        $this->client = $rowJob['client'];
        $this->place = $rowJob['place'];
        $this->description = $rowJob['description'];
        $this->title = $rowJob['title'];

        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of product to be updated
        $stmt->bindParam(":idJob", $this->id);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // set values to object properties
        foreach($row as $line){
            $this->photos[] =[ 
                'name' => $line['name'],
                'client'=> $line['client'],
                'place' => $line['place'],
                'description' => $line['description'],
                'job_type_id' => $line['job_type_id'],
                'filename' => $line['filename'],
                'job_id' => $line['job_id'],
            ];
        }
    }

    // used when filling up the update product form
    function readOnePhoto(){
    
        $query = "SELECT j.id, j.name, j.client, j.place, j.job_type_id, jp.filename, jp.description, jp.job_id
                FROM job_photos AS jp 
                JOIN ".$this->table_name." AS j  ON " .$this->id. " = jp.job_id 
                WHERE  j.id = :idJob AND jp.highlight = 1;
                ";

        $queryJob = "SELECT j.id, j.name, j.client, j.place, j.job_type_id, j.description, t.title
                FROM job AS j 
                INNER JOIN job_type t ON j.job_type_id = t.id
                WHERE  j.id = :idJob
                ";
        $stmtJob = $this->conn->prepare( $queryJob );
        $stmtJob->bindParam(":idJob", $this->id);
        $stmtJob->execute();
        $rowJob = $stmtJob->fetch(PDO::FETCH_ASSOC);
        
        $this->id = $rowJob['id'];
        $this->name = $rowJob['name'];
        $this->client = $rowJob['client'];
        $this->place = $rowJob['place'];
        $this->description = $rowJob['description'];
        $this->title = $rowJob['title'];

        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind id of product to be updated
        $stmt->bindParam(":idJob", $this->id);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // set values to object properties
        foreach($row as $line){
            $this->photos[] =[ 
                'name' => $line['name'],
                'client'=> $line['client'],
                'place' => $line['place'],
                'description' => $line['description'],
                'job_type_id' => $line['job_type_id'],
                'filename' => $line['filename'],
                'job_id' => $line['job_id'],
            ];
        }
    }

    // update the product
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    name = :name,
                    job_type_id = :job_type_id,
                    description = :description,
                    client = :client
                WHERE
                    id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->job_type_id=htmlspecialchars(strip_tags($this->job_type_id));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->client=htmlspecialchars(strip_tags($this->client));
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind new values
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':job_type_id', $this->job_type_id);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':client', $this->client);
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // delete the product
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    // search job
    function search($keywords){
    
        // select all query
        $query = "SELECT
                    c.name as category_name, p.id, p.name, p.description, p.job_type_id, p.client, p.created
                FROM
                    " . $this->table_name . " p
                    LEFT JOIN
                        categories c
                            ON p.client = c.id
                WHERE
                    p.name LIKE ? OR p.description LIKE ? OR c.name LIKE ?
                ORDER BY
                    p.created DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $keywords=htmlspecialchars(strip_tags($keywords));
        $keywords = "%{$keywords}%";
    
        // bind
        $stmt->bindParam(1, $keywords);
        $stmt->bindParam(2, $keywords);
        $stmt->bindParam(3, $keywords);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // read job with pagination
    public function readPaging($from_record_num, $records_per_page){
    
        // select query
        $query = "SELECT
                    c.name as category_name, p.id, p.name, p.description, p.job_type_id, p.client, p.created
                FROM
                    " . $this->table_name . " p
                    LEFT JOIN
                        categories c
                            ON p.client = c.id
                ORDER BY p.created DESC
                LIMIT ?, ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
    
        // execute query
        $stmt->execute();
    
        // return values from database
        return $stmt;
    }

    // used for paging job
    public function count(){
        $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }
}
