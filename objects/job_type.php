<?php
class JobType{
 
    // database connection and table name
    private $conn;
    private $table_name = "job_type";
 
    // object properties
    public $id;
    public $title;
    public $created;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // used by select drop-down list
    public function readAll(){
        //select all data
        $query = "SELECT
                    id, title
                FROM
                    " . $this->table_name . "
                ORDER BY
                    title";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }

    // used by select drop-down list
    public function read(){
    
        //select all data
        $query = "SELECT
                    id, title
                FROM
                    " . $this->table_name . "
                ORDER BY
                    title";
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
    
        return $stmt;
    }

    // create product
    function create(){
    
      // query to insert record
      $query = "INSERT INTO
                  " . $this->table_name . "
              SET
                  title=:title, created=:created";
  
      // prepare query
      $stmt = $this->conn->prepare($query);
  
      // sanitize      
      $this->title=htmlspecialchars(strip_tags($this->title));
      $this->created=htmlspecialchars(strip_tags($this->created));
  
      // bind values
      $stmt->bindParam(":title", $this->title);
      $stmt->bindParam(":created", $this->created);
  
      // execute query
      if($stmt->execute()){
          return true;
      }
  
      return false;
   
      
    }
    // delete the product
    function delete(){
    
      // delete query
      $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
  
      // prepare query
      $stmt = $this->conn->prepare($query);
  
      // sanitize
      $this->id=htmlspecialchars(strip_tags($this->id));
  
      // bind id of record to delete
      $stmt->bindParam(1, $this->id);
  
      // execute query
      if($stmt->execute()){
          return true;
      }
  
      return false;
      
  }
}
?>