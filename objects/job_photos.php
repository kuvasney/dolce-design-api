<?php
class JobPhotos{
 
    // database connection and table name
    private $conn;
    private $table_name = "job_photos";
 
    // object properties
    public $id;
    public $filename;
    public $description;
    public $job_id;
    public $highlight;
    public $created;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // used by select drop-down list
    public function read(){
    
        //select all data
        $query = "SELECT
                    id, description
                FROM
                    " . $this->table_name . "
                ORDER BY
                    description";
    
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
    
        return $stmt;
    }

    // create product
    function create(){
    
      // query to insert record
      $query = "INSERT INTO " . $this->table_name . " (filename, description, job_id, highlight, created) VALUES (:filename, :description, :job_id, :highlight, :created)";
  
      // prepare query
      $stmt = $this->conn->prepare($query);
  
      // sanitize      
      $this->filename=htmlspecialchars(strip_tags($this->filename));
      $this->description=htmlspecialchars(strip_tags($this->description));
      $this->job_id=htmlspecialchars(strip_tags($this->job_id));
      $this->created=htmlspecialchars(strip_tags($this->created));
      $this->description=htmlspecialchars(strip_tags($this->description));

      // bind values
      $stmt->bindParam(":filename", $this->filename);
      $stmt->bindParam(":description", $this->description);
      $stmt->bindParam(":highlight", $this->highlight);
      $stmt->bindParam(":job_id", $this->job_id);
      $stmt->bindParam(":created", $this->created);
  
      // execute query
      if($stmt->execute()){
          return true;
      }
  
      return false;
   
      
    }
    // delete the product
    function delete(){
    
      // delete query
      $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
  
      // prepare query
      $stmt = $this->conn->prepare($query);
  
      // sanitize
      $this->id=htmlspecialchars(strip_tags($this->id));
  
      // bind id of record to delete
      $stmt->bindParam(1, $this->id);
  
      // execute query
      if($stmt->execute()){
          return true;
      }
  
      return false;
      
  }
}
?>