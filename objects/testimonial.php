<?php
class Testimonial{
 
    // database connection and table name
    private $conn;
    private $table_name = "testimonials";
 
    // object properties
    public $id;
    public $testimonial;
    public $author;
    public $job;
    public $created;
 
    public function __construct($db){
        $this->conn = $db;
    }
 
    // create new user record
    function create(){
    
        $query = "INSERT INTO " . $this->table_name . " (testimonial, author, job) VALUES (:testimonial, :author, :job)";
    
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize        
        $this->testimonial=htmlspecialchars(strip_tags($this->testimonial));
        $this->author=htmlspecialchars(strip_tags($this->author));
        $this->job=htmlspecialchars(strip_tags($this->job));
    
        // bind the values        
        $stmt->bindParam(':testimonial', $this->testimonial);        
        $stmt->bindParam(':author',$this->author);
        $stmt->bindParam(':job', $this->job);
    
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    //read records
    function read(){
    
        // select all query
        $query = "SELECT id, testimonial, author, job, created, modified FROM " . $this->table_name . ""; 

        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // update testimonial
    function update(){
    
        // update query
        $query = "UPDATE " . $this->table_name . " SET testimonial = :testimonial, author = :author, job = :job  WHERE  id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->testimonial=htmlspecialchars(strip_tags($this->testimonial));
        $this->author=htmlspecialchars(strip_tags($this->author));
        $this->job=htmlspecialchars(strip_tags($this->job));
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind new values
        $stmt->bindParam(':testimonial', $this->testimonial);
        $stmt->bindParam(':author', $this->author);
        $stmt->bindParam(':job', $this->job);
        $stmt->bindParam(':id', $this->id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // delete the product
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }
}
?>