<?php
// required headers
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') { 
    header('Access-Control-Allow-Origin: *'); 
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS'); 
    header("Access-Control-Allow-Headers: *");
    header('Access-Control-Max-Age: 1728000'); 
    header('Content-Length: 0'); 
    // header('Content-Type: text/plain'); 
    header('Content-type: application/json');
    die(); 
} 
header('Access-Control-Allow-Origin: *'); 
header('Content-Type: application/json'); 
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS'); 
header("Access-Control-Allow-Headers: *");
 
// include database and object file
include_once '../config/db.php';
include_once '../objects/testimonial.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare testimonial object
$testimonial = new testimonial($db);
 
// get testimonial id
$data = json_decode(file_get_contents("php://input"));
 
// set testimonial id to be deleted
$testimonial->id = $data->id;
 
// delete the testimonial
if($testimonial->delete()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "Depoimento removido."));
}
 
// if unable to delete the testimonial
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Não foi possível remover o depoimento."));
}
?>