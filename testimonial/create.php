<?php
// required headers
include_once '../config/headers.php';
 
// files needed to connect to database
include_once '../config/db.php';
include_once '../objects/testimonial.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate testimonial object
$testim = new Testimonial($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set testimonial property values
$testim->testimonial = $data->testimonial;
$testim->author = $data->author;
$testim->job = $data->job;
 
// create the user
if(
    !empty($testim->testimonial) &&
    !empty($testim->author) &&
    $testim->create()
){
 
    // set response code
    http_response_code(200);
 
    // display message: testimonial was created
    echo json_encode(
        array(
            "message" => "Depoimento criado!",
            "testimonial" => $testim->testimonial,
            "author" => $testim->author,
            "job" => $testim->job
        )
    );
}
 
// message if unable to create testimonial
else{
 
    // set response code
    http_response_code(400);
 
    // display message: unable to create testimonial
    echo json_encode(array("message" => "Não foi possível criar o depoimento"));
}
?>