<?php
// required headers
include_once '../config/headers.php';
 
// get database connection
include_once '../config/db.php';
 
// instantiate job_type object
include_once '../objects/job_type.php';
 
$database = new Database();
$db = $database->getConnection();
 
$job = new JobType($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if(
    !empty($data->title)
){
 
    // set job property values
    $job->title = $data->title;
    $job->created = date('Y-m-d H:i:s');
 
    // create the job
    if($job->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "tipo de obra criado."));
    }
 
    // if unable to create the job, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create job."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create job. Data is incomplete."));
}
?>