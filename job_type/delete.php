<?php
// required headers
include_once '../config/headers.php';
 
// include database and object file
include_once '../config/db.php';
include_once '../objects/job_type.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare job_type object
$job_type = new JobType($db);
 
// get job_type id
$data = json_decode(file_get_contents("php://input"));
 
// set job_type id to be deleted
$job_type->id = $data->id;
 
// delete the job_type
if($job_type->delete()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "tipo de obra removido."));
}
 
// if unable to delete the job_type
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Não foi possível remover o tipo de obra."));
}
?>