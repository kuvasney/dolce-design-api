<?php
// required headers
include_once '../config/headers.php';
 
// include database and object files
include_once '../config/db.php';
include_once '../objects/job_type.php';
 
// instantiate database and job_type object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$job_type = new JobType($db);
 
// query job_types
$stmt = $job_type->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // job_types array
    $job_types_arr=array();
    $job_types_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $job_type_item=array(
            "id" => $id,
            "title" => html_entity_decode($title)
        );
 
        array_push($job_types_arr["records"], $job_type_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show job_types data in json format
    echo json_encode($job_types_arr);

} else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no job_types found
    echo json_encode(
        array("message" => "No job_types found.")
    );
}