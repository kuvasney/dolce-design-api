<?php
    // required headers
    include_once './config/headers.php';

    require("libs/PHPMailer-master/src/PHPMailer.php");
    require("libs/PHPMailer-master/src/SMTP.php");
    
    // get posted data
    $data = json_decode(file_get_contents("php://input"));

    // print_r($data);
    // die();

    $mail = new PHPMailer\PHPMailer\PHPMailer();
    $mail->IsSMTP(); // enable SMTP
    $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
    $mail->Host = "mail.dolce-d.com.br";
    $mail->Port = 465; // or 587
    $mail->IsHTML(true);
    $mail->Username = "contato@dolce-d.com.br";
    $mail->Password = ""; //SETAR UM PASSWORD
    $mail->SetFrom("contato@dolce-d.com.br");
    // $mail->SetFrom($data->email);
    $mail->Subject = "Contato via site Dolce-d";
    $mail->Body = $data->message . " <br>Nome: " . $data->name . "<br> E-mail: " . $data->email . "<br> Telefone: " . $data->phone;
    $mail->AddAddress("jessica@dolce-d.com.br");
        if(!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
        echo "Mensagem enviada com sucesso";
        }
?>